from django.shortcuts import (
    render,get_object_or_404,
    )
from django.contrib.auth.decorators import login_required
from social_django.models import UserSocialAuth # Getting the access_tokens

from pages.models import Pages # For Storing and Updating Pages 

from pages.forms import PageForm # For Updating the Page From Modal

import requests # Make the Graph API Request

from django.http import JsonResponse
from django.template.loader import render_to_string

graph_api = "https://graph.facebook.com/"
page_list_url = graph_api + "{id}/accounts?access_token={token}" # noqa
page_details_url = graph_api + "{id}?fields=about,attire,bio,location,parking,impressum,emails,website,name&access_token={token}" # noqa
page_update_url = graph_api + "{id}?bio={bio}&access_token={token}&about={about}&impressum={impressum}"



def get_pages(token,id):
    # Graph API GET /accounts
    """
    This end point retrive all the pages that,
    User Shared with us during the auth.
    
    param : retrive it from django social models

    token = acces_token
    id = user_id
    """
    url = page_list_url.format(
        id = id,
        token = token
    )
    print(url)
    resp = requests.request("GET", url)
    if resp.status_code == 200:
        return resp.json()
    return None

def get_page_detail(page):
    # Graph API GET /<pageid>
    """
    This end point retrive all the info about 
    specific page.
    
    param : retrive it from pages model
    
    page = object hold id and token
    """
    url = page_details_url.format(
        id = page.pageid,
        token = page.token
    )
    print(url)
    resp = requests.request("GET", url)
    print(resp.text)
    if resp.status_code == 200:
        return resp.json()
    return None

def update_page_detail(page):
    # Graph API POST /<pageid>
    """
    This end point update all the info about 
    specific page.
    
    param : retrive it from pages model
    
    page = object hold id and token
    """
    url = page_update_url.format(
        id=page.pageid,
        about=page.about,
        bio=page.bio,
        impressum=page.impressum,        
        token=page.token
    )
    resp = requests.request("POST", url)
    print(resp.text)
    if resp.status_code == 200:
        return resp.json()
    return None


@login_required
def pages(request):
    # Views /pages
    """
    This end point will list, 
    all the pages that user shared during auth

    """
    social_profile=get_object_or_404(UserSocialAuth,user=request.user,provider='facebook') # Retriving for access_token and user id
    token = social_profile.extra_data['access_token']
    id = social_profile.uid
    pages = get_pages(token,id)

    pids = [] # Later will check and update page list with access

    for page in pages['data']:
        """
        Store the pages details to page model
        """
        pids.append(page['id'])
        page_entry,created = Pages.objects.update_or_create(
            pageid=page['id'] ,user=social_profile.user,
            defaults={
                'name': page['name'],
                'category': page['category'],
                'token': page['access_token']
                }
        )


        detail = get_page_detail(page_entry) # Retriving Meta Data about page
        if detail:
            if 'about' in detail:
                page_entry.about = detail['about']
            if 'bio' in detail:
                page_entry.bio = detail['bio']
            if 'impressum' in detail:
                page_entry.impressum = detail['impressum']
            page_entry.extra_data = detail
            page_entry.save()
    
    page_list = Pages.objects.filter(user=request.user).filter(pageid__in=pids) # Updated list

    pages_removed = Pages.objects.filter(user=request.user).exclude(pageid__in=pids)
    if pages_removed: # Page will be removed if the access is denied
        pages_removed.delete()
    return render(
        request, 'page_list.html',
        context={
        "pages":page_list
        }
    )



def save_page_form(request, form, template_name,pageid):
    #Form data saved to page model
    """
    Process the Page Updation Form
    Make the API request to  update_page_detail
    param: 
        pageid :to update that specific page
        form: formdata
        template_name : template renderd after form submission  
    """
    data = dict()
    social_profile=get_object_or_404(UserSocialAuth,user=request.user,provider='facebook')
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            page = get_object_or_404(Pages,pageid=pageid)
            update_page_detail(page)
            data['form_is_valid'] = True
            pages = Pages.objects.filter(user=social_profile.user)
            data['html_pages_list'] = render_to_string('includes/partial_page_list.html', {
                "pages":pages
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)




def page_update(request, pageid):
    #Views pages/<pageid>/update/
    """
    Process the Page Updation Form
    param: 
        pageid :to update that specific page  
    """
    page = get_object_or_404(Pages, pageid=pageid)
    if request.method == 'POST':
        form = PageForm(request.POST, instance=page)
    else:
        form = PageForm(instance=page)
    return save_page_form(request, form, 'includes/partial_page_update.html',pageid)