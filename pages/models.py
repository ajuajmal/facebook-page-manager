from django.db import models
from django.contrib.auth.models import User


# Store Each Page Entry based on users.
# token - page access token
# pageid - pageid
# *others = meta data

class Pages(models.Model):
    provider = models.CharField(max_length=255, blank=True, default='Facebook')
    name = models.CharField(max_length=255)
    category = models.CharField(max_length=255, blank=True)
    about = models.CharField(max_length=255, blank=True)
    bio = models.CharField(max_length=255, blank=True)
    impressum = models.CharField(max_length=255, blank=True, null=True)
    pageid = models.CharField(max_length=255, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    extra_data = models.JSONField(blank=True,null=True)
    token = models.CharField(max_length=1000, blank=True)
    def __str__(self):
        return self.name
    