$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    console.log('formerr')

    
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-pages .modal-content").html("");
        $("#modal-pages").modal("show");
      },
      success: function (data) {
        $("#modal-pages .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#pages-table tbody").html(data.html_pages_list);
          $("#modal-pages").modal("hide");
        }
        else {
          $("#modal-pages .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */
  console.log('hey')

  // Update pages
  $("#pages-table").on("click", ".js-update-page", loadForm);
  $("#modal-pages").on("submit", ".js-pages-update-form", saveForm);

});
