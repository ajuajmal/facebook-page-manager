from django import forms

from pages.models import Pages


# PageMeta Data Update
class PageForm(forms.ModelForm):
    class Meta:
        model = Pages
        fields = ('about', 'bio','impressum')