from django.shortcuts import render
from django.contrib.auth.decorators import login_required

def login(request):
  #Views /login
  """
  FB Django Social Integration : Auth
  """
  return render(request, 'login.html')
  
@login_required
def home(request):
  #Views /
  """
  FB Django Social Integration : Data
  """
  return render(request, 'home.html')