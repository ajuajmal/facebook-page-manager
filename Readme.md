## FB Page Manager

> https://fbapp.ajmalaju.com/

Test User 1:- 

email : `efgfmpctyr_1620858940@tfbnw.net`
password: `test@321`

Test User 2:- 

email: `zexsaastlq_1620858940@tfbnw.net`
password: `test@321`


##### Django 3.2.2

##### FB Graph API

##### Django Social Auth

##### Gitlab CD Pipelines

##### Heroku - POSTGRES+SENTRY

### Run

- `git clone git@gitlab.com:ajuajmal/facebook-page-manager.git`

- `python3 -m venv env`

- `. env/bin/activate`

- `pip3 install -r requirements.local.txt`

- if docker and docker and docker-compose available `docker-compose -f db.yaml up -d`

    - else define local/remote db url in .env.local in this format `DATABASE_URL=postgres://USER:PASSWORD@HOST:PORT/NAME` or `sqlite:///sqlite3.db`

- `python3 manage.py makemigrations`

- `python3 manage.py migrate`

- `python3 manage.py runserver --settings=fb_app.settings.local`


## Production

### Heroku

 - `heroku login`

 - `heroku create --buildpack https://github.com/heroku/heroku-buildpack-python`
 
    - out  `Creating app... done, ⬢ your-app-name`

- `heroku git:remote -a your-app-name`

- `heroku addons:create heroku-postgresql:hobby-dev`

- `heroku pg:backups schedule --at '02:00 America/Los_Angeles' DATABASE_URL`

- `heroku pg:promote DATABASE_URL`

- `heroku config:set DEBUG=False`

- `heroku config:set DJANGO_SETTINGS_MODULE=fb_app.settings.production`

- `heroku config:set DJANGO_SECRET_KEY="$(openssl rand -base64 64)"` 

- `heroku config:set DJANGO_ALLOWED_HOSTS=your-app-name.herokuapp.com`

- `heroku addons:create sentry:f1`

- `heroku config:set SOCIAL_AUTH_FACEBOOK_KEY=`

- `heroku config:set SOCIAL_AUTH_FACEBOOK_SECRET=`

- optional : `heroku config:set DJANGO_SECURE_SSL_REDIRECT=False` fo adding a free custom ssl domain via cloudflare
